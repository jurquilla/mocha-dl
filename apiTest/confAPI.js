const path = require('path');

exports.config = {
    directConnect: true,
    capabilities: {
      browserName: 'chrome',
    
      chromeOptions: {
         args: [ "--headless", "--disable-gpu"]
       }
    },
    framework: 'mocha',
    beforeLaunch: function () {
        require('ts-node').register({
            project: '../tsconfig.json'
        });
    },

    specs: ['test/apiTest01.ts'],

    onPrepare: () => {

//REPORTERIA
var chai = require("chai");
var chaiAsPromised = require("chai-as-promised");
chai.use(chaiAsPromised);
global.chai = chai;

  },
  mochaOpts: {
    timeout: 5000, ui: 'bdd',     reporter: 'mochawesome',
    reporterOptions: {
      overwrite: true,
      json: false
    }
}


}