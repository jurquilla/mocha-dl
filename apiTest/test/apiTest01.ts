import axios from "axios";

describe('Full Case Studie Creation', async () => {

  axios.defaults.baseURL = 'https://maui-dev-api.value-cloud.com';
 // let AUTH_TOKEN;
  let token: string, id_caseStudy: any, id_testimonial: any, id_benefit: any, id_section: any;
  let headers: { Authorization: string; };

  before(async () => {

    // try {
    
    const response = await axios.post('/oauth/token', {
      "grant_type": "password",
      "client_id": 2,
      "client_secret": "6q0AgyVd2qZQzzBv76BeZKfwvHKdyfRKFrLiGeJX",
      "username": "admin@demo.com",
      "password": "D3m0U$3r"
    });

      token = "Bearer " + response.data.access_token;
     // AUTH_TOKEN = response.data.access_token;

     // console.log(token);
      headers = {
        "Authorization": token
    };
    chai.expect(response.status).to.equal(200);
  // } catch (error) {
  //   console.error(error);
  // }
  });
  
  //axios.defaults.headers.common['Authorization'] = AUTH_TOKEN;
  
  it('Creating case studie', async () => {
      const response = await axios({
        method: 'POST',
        url: '/api/case_studies',
        headers: headers,
        data: {
          "company_id": 73,
          "name": "Case Study - Test Protractor Axios",
          "sequence": 0,
          "description": "Api test Axios",
          "tags": "Axios NPM",
          "redacted": 0,
          "active": 1,
          "default_off": 0
        }
      });
      //201
      id_caseStudy = response.data.data.id;
      chai.expect(response.status).to.equal(205);
      //console.log(response.data);
      //console.log(id_caseStudy);
      //console.log(response.data.access_token);

  });

  it('Creating a testimonial for the case study', async () => {

      const response = await axios({
        method: 'POST',
        url: '/api/case_study_testimonials',
        headers: headers,
        data: {
          "sequence": 0,
          "case_study_id": id_caseStudy,
          "contact_id": 11,
          "description": "Testimonial done with Axios"
        }
      });
      id_testimonial = response.data.data.id;
      chai.assert.equal(response.status,201);

      //console.log(response.data);
      //console.log(id_testimonial);
      //console.log(response.data.access_token);
 
  });

  it('Creating a section for the case study', async () => {

      const response = await axios({
        method: 'POST',
        url: '/api/case_study_sections',
        headers: headers,
        data: {
           "sequence": 1,
           "case_study_id": id_caseStudy,
           "case_study_section_type_id": 121,
           "description": "Section done with Axios"
        }
      });
      id_section = response.data.data.id;
      chai.expect(response.status).to.equal(201);

      //console.log(response.data);
      //console.log(id_section);
      //console.log(response.data.access_token);

  });

  it('Creating a benefit for the case study', async () => {

      const response = await axios({
        method: 'POST',
        url: '/api/case_study/' + id_caseStudy + '/benefits',
        headers: headers,
        data: {
          "benefits": [
            {
              "value": 10,
              "unit_type_id": 1,
              "benefit_id": 481
            }
          ]
        }
      });

      //console.log(response.data);
    id_benefit = response.data.data.case_study_benefits[0].case_study_benefit.benefit_id;
    chai.expect(response.status).to.equal(200);
     // console.log(id_benefit);
      //console.log(response.data.access_token);

  });

  	
  after(async () => {
    //try {

      const response1 = await axios({
        method: 'DELETE',
        url: '/api/case_study_testimonials/' + id_testimonial,
        headers: headers
      });
      const response2 = await axios({
        method: 'DELETE',
        url: '/api/case_study_sections/' + id_section,
        headers: headers
      });

      const response3 = await axios({
        method: 'DELETE',
        url: '/api/case_study/' + id_caseStudy + '/benefits/' + id_benefit,
        headers: headers
      });

      const response4 = await axios({
        method: 'DELETE',
        url: '/api/case_studies/' + id_caseStudy,
        headers: headers
      });


      chai.expect(response1.status).to.equal(204);
      chai.expect(response2.status).to.equal(204);
      chai.expect(response3.status).to.equal(204);
      chai.expect(response4.status).to.equal(204);

      // console.log(response1.data);
      // console.log(response2.data);
      // console.log(response3.data);
      // console.log(response4.data);

  // } catch (error) {
  //   console.error(error);
  // }

  });






});
