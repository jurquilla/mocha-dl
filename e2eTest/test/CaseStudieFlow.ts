//Usually import on TS
//import { expect } from "chai";
import { Home } from "../pages/Home";


describe('Case Studie', function () {
  //describe('Case Studie', async () => {


  const should = chai.should();
  let name = "Test Case Study Protractor";

  it('Click on tab Case Studies', async () => {

    Home.variables("TabCaseStudies").click();

  });

  it('Click on (+ Add New) Case Studie', async () => {

    Home.variables("AddCaseStudie").click();

  });

  it('Creating new Case Study', async () => {

    Home.createCaseStudy("YES", "price", "625", name);
   // Home.variables("Alert").getText().should.eventually.equal(' Case Study was successfully saved. ');
    chai.expect(Home.variables("Alert")).to.exist;

  });


  it('Adding Testimonials', async () => {
    Home.addTestimonial("Josue", "Urquilla", "Test Testimony!!!");
    chai.expect(Home.variables("Alert")).to.exist;
  

  });

  it('Adding Sections', async () => {
    Home.addSection("110", "Test Description!!!", "https://www.youtube.com/watch?v=7-IYx7kS8Ww", "Media Description!!!");
    chai.expect(Home.variables("Alert")).to.exist;
  });
  

  it('Adding Benefits', async () => {
    Home.addBenefit("churn");
    chai.expect(Home.variables("Alert")).to.exist;


  });
  
  
  after(() => {
    Home.cleanUp(3);
    chai.expect(Home.variables("Alert")).to.exist;

  });

});
