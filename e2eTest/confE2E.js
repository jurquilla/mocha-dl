const path = require('path');

exports.config = {

  directConnect: true,
  // sauceUser: "applaudito" ,
  // sauceKey: "8af04450-4530-41af-a15f-51e7da5f04f1" ,
  // sauceRegion: "us-west-1" ,
   framework: 'mocha',
  mochaOpts: {
      timeout: 50000, ui: 'bdd',  reporter: 'mochawesome-screenshots',
      reporterOptions: {
        overwrite: true,
        takePassedScreenshot: true,
        clearOldScreenshots: true,
        shortScrFileNames: false,
        jsonReport: false,
        multiReport: false 
      }
  },
  capabilities: {
     'browserName': 'chrome' ,
    },
  
  beforeLaunch: function () {
    require('ts-node').register({
      project: '../tsconfig.json'
    });
  },
  onPrepare: async () => {
    var globals = require("protractor");

    browser.driver.get("https://maui-dev.value-cloud.com/");
    //ONLY FOR CHROME WHEN TEST FAIL ON CLICK INTERCEPTABLE
    browser.manage().window().setSize(1600, 1000);
    //browser.manage().window().maximize();
    //Only for non-angular applications
    await browser.waitForAngularEnabled(true);
    //browser.ignoreSynchronization = false;
    //Until herezo
    global.browser = globals.browser;


    var chai = require("chai");
    var chaiAsPromised = require("chai-as-promised");
    chai.use(chaiAsPromised);
    global.chai = chai;



    // return browser.getProcessedConfig().then(function(config) {
    //   if (config.specs.length > 0) {
    //     const spec = path.basename(config.specs[0]);

    //     process.env.MOCHAWESOME_REPORTTITLE = spec;
    //     process.env.MOCHAWESOME_REPORTFILENAME = spec;
      
    //   } });
    
  },


  specs: ['test/LoginFlow.ts', 'test/CaseStudieFlow.ts'],



}